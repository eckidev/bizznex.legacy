<?php
/**
 * Created by JetBrains PhpStorm.
 * User: micael.bergeron
 * Date: 06/05/14
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */

namespace Bizznex\Security;
class AuthenticationHelper {
    /**
     * Hashes a password using Blowfish and returns the salt-hashed_password string.
     *
     * @param $value string The raw password to be hashed.
     *
     * @return string
     */
    static function password($clear)
    {
        $salt = "$2a$10$".AuthenticationHelper::generateSalt(22);
        return crypt($clear, $salt);
    }

    /**
     * Generate a blowfish compatible random string
     */
    static function generateSalt($length)
    {
        if (!defined('MCRYPT_DEV_URANDOM')) throw new Exception('The MCRYPT_DEV_URANDOM source is required (PHP 5.3).');
        $binaryLength = (int)($length * 3 / 4 + 1);
        $randomBinaryString = mcrypt_create_iv($binaryLength, MCRYPT_DEV_URANDOM);
        $randomBase64String = base64_encode($randomBinaryString);

        // blowfish's alphabet is using '.' instead of '+'
        $randomBase64String = str_replace('+', '.', $randomBase64String);
        return substr($randomBase64String, 0, $length);
    }

    /*
    * This function is backward compatible with md5 hashes.
    */
    static function match($clear, $hashed)
    {
        if (strpos($hashed, "$2") === FALSE)
        {
            // try the md5 hashing test.
            // trigger_error("Using MD5 inference in password matching.", E_WARNING);
            return md5($clear) == $hashed;
        }

        // crypt will only us the first 22 characters of the salt, which is the salt used.
        $h = crypt($clear, $hashed);
        return $h == $hashed;
    }
}
