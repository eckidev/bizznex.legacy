<?php

namespace Bizznex\Utils;

use DateTime;

class HtmlHelper
{
    // if you change one of these, don't forget the one in past\inc\fonct_valide_date.js
    public static $defaultTimeFormat = "Y-m-d H:i"; // http://ca1.php.net/manual/en/datetime.createfromformat.php
    public static $defaultTimeFormat_js = "yyyy-mm-dd hh:ii"; // http://www.malot.fr/bootstrap-datetimepicker/
    public static $defaultTimeFormat_momentjs = "YYYY-MM-DD hh:mm"; // http://momentjs.com/

    public static function __callStatic($name, $args)
    {
        ob_start();
        switch ($name) {
            case "recherche_alpha":
                self::recherche_alpha($args[0], $args[1]);
                break;
            case "title":
                self::title($args[0], $args[1]);
                break;
        }
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    /**
     * @param string $datetimeString
     *
     * @return DateTime
     */
    public static function createDefaultDateTime($datetimeString)
    {
        return DateTime::createFromFormat(self::$defaultTimeFormat, $datetimeString);
    }

    public static function datetime_from_params($y, $m, $d, $h)
    {
        $now = new DateTime();
        $year = isset($y) ? $y : $now->format("Y");
        $month = isset($m) ? $m : $now->format("m");
        $day = isset($d) ? $d : $now->format("d");
        if (isset($h)) {
            $time = explode(":", $h);
            $hour = $time[0];
            $minute = $time[1];
        } else {
            $hour = $now->format("H");
            $minute = $now->format("i");
        }

        return new DateTime($year . "-" . $month . "-" . $day . " " . $hour . ":" . $minute);
    }

    public static function datetime_from_due($datedue, $heurdue, $minutedue)
    {
        $date = new DateTime();
        $date->setTimestamp($datedue);
        return new DateTime($date->format("Y") . "-" . $date->format("m") . "-" . $date->format("d") . " " . $heurdue . ":" . $minutedue);
    }

    /**
     * datetimepicker legacy support for bootstrap styled datepicker, fills date in three separated inputs
     * http://www.malot.fr/bootstrap-datetimepicker/
     *
     * @param string   $code_lang
     * @param string   $date_to   form input name
     * @param string   $hour_to   form input name
     * @param string   $minute_to form input name
     * @param DateTime $date      initial date value
     */
    public static function date_time_picker($code_lang = "fr", $date_to = "date_contactactivite", $hour_to = "heur_contactactivite", $minute_to = "minute_contactactivite", $date = null)
    {
        $date_formatted = isset($date) && $date ? date_format($date, self::$defaultTimeFormat) : null;
        ?>
        <div class="input-group entree_select">
            <input size="16" type="text" value="<?= $date_formatted ?>" class="form_datetime form-control" rel="datetimepicker" data-date-to="<?= $date_to ?>" data-hour-to="<?= $hour_to ?>" data-minute-to="<?= $minute_to ?>" data-date-language="<?= $code_lang ?>" data-date-format="<?= self::$defaultTimeFormat_js ?>" data-momentjs-format="<?= self::$defaultTimeFormat_momentjs ?>">
            <span class="input-group-addon"><i class="icon-calendar"></i></span>
        </div>
        <!-- Legacy suppport -->
        <input type="hidden" id="<?= $date_to ?>" name="<?= $date_to ?>"/>
        <input type="hidden" id="<?= $hour_to ?>" name="<?= $hour_to ?>"/>
        <input type="hidden" id="<?= $minute_to ?>" name="<?= $minute_to ?>"/>
    <?php
    }

    /**
     * datetimepicker legacy support for bootstrap styled datepicker, fills datetime in only one parameter
     * http://www.malot.fr/bootstrap-datetimepicker/
     *
     * @param string   $code_lang
     * @param string   $date_to form input name
     * @param DateTime $date            initial date value
     */
    public static function date_picker($code_lang = "fr", $date_to = "date_contactactivite", $date = null, $options = array())
    {
        $options = array_merge(array(
            "class" => "form_datetime",
            "icon" => "icon-calendar",
            "format" => self::$defaultTimeFormat,
            "js_format" => self::$defaultTimeFormat_js,
            "momentjs_format" => self::$defaultTimeFormat_momentjs
        ), $options);
        $date_formatted = isset($date) && $date 
            ? date_format($date, $options["format"]) 
            : null;
        ?>
        <div class="input-group entree_select">
            <input size="16" type="text" value="<?= $date_formatted ?>" class="<?= $options['class'] ?> form-control" 
                rel="datetimepicker" data-datetime-to="<?= $date_to ?>" data-date-language="<?= $code_lang ?>" 
                data-date-format="<?= $options['js_format'] ?>" data-momentjs-format="<?= $options['momentjs_format']  ?>">
            <span class="input-group-addon"><i class="<?= $options['icon'] ?>"></i></span>
        </div>
        <!-- Legacy suppport -->
        <input type="hidden" id="<?= $date_to ?>" name="<?= $date_to ?>" />
    <?php
    }


    /**
     * new multiple select field for user selection
     */
    public static function select_multiple_users(array $selected_users_id = array())
    {
        ?>
        <select multiple="multiple" class="multi-select multiple-account" id="allUsers" name="allUsers[]">
            <?php
            $Req_select = mysql_query("SELECT * FROM t_utilisateurs WHERE is_desactive_utilisateur = '0' ORDER BY prenom_utilisateur");
            if (mysql_num_rows($Req_select) > 0) {
                while ($Resselect = mysql_fetch_array($Req_select)) {
                    /** @global $connexion string */
                    if (get_groupe_principal($connexion, $Resselect['id_utilisateur']) != 'ROOT') {
                        $selected = array_search($Resselect['id_utilisateur'], $selected_users_id) !== FALSE ? 'selected="selected"' : null;
                        ?>
                        <option value="<?= $Resselect['id_utilisateur'] ?>" <?= $selected ?>>
                            <?= $Resselect['prenom_utilisateur'] . " " . $Resselect['nom_utilisateur'] ?>
                        </option>
                    <?php
                    }
                }
            }
            ?>
        </select>
    <?php
    }

    /**
     * new multiple select field for contact selection
     */
    public static function select_multiple_contacts($doorman, array $selected_contacts_id = array())
    {
        ?>
        <select multiple="multiple" class="multi-select multiple-account" id="allUsers" name="allUsers[]">
            <?php
            $sql_contact = "SELECT * FROM t_contact WHERE ( type_contact = 1 OR utilisateur_id_contact = " . $doorman->id_utilisateur . " ) ORDER BY prenom_contact, nom_contact";
            $req_contact = mysql_query($sql_contact);
            while ($tab_contact = mysql_fetch_array($req_contact)) {
                $selected = array_search($tab_contact['id_contact'], $selected_contacts_id) !== FALSE ? 'selected="selected"' : null;
                ?>
                <option value="<?= $tab_contact['id_contact'] ?>" <?= $selected ?>>
                    <?= $tab_contact['prenom_contact'] ?> <?= $tab_contact['nom_contact'] ?>
                </option>
            <?php
            }

            ?>
        </select>
    <?php
    }

    /**
     * Previously used MM_validate to validate inputs, updated valider_pour_qui to work with new user selector
     * Requires to manually use some kind of weird MM_Validate stuff...
     * @todo: use bootstrap required fields (requires bootstrap forms...), see http://jsfiddle.net/5WMff/
     *
     */
    public static function javascript_validator($checkbox_name = "pour_qui_tache", $form_selector = "#main_form")
    {
        ?>
        <script language="JavaScript" type="application/javascript">
            var img = document.createElement('img');
            img.src = '../img/icones/loader.gif';

            function isEmpty(str) {
                return !!(str == null || str == "");
            }

            function valider_pour_qui() {
                var destinataire = $("input:radio[name='<?=$checkbox_name?>']:checked").val();

                if (destinataire == 1 || destinataire == 2 || destinataire == 3) {
                    return true;
                }

                <?php //valide seulement si un ou plusieurs destinataires sont selectionnes ?>
                if (destinataire == 4) {
                    return ($("<?=$form_selector?>").find("#allUsers").val().length >= 1);
                }

                if (destinataire == 5) {
                    return ($("#input-group-departement :checkbox:checked").length > 0)
                }
            }

            $(document).ready(function () {
                $('#allUsers').multiSelect({
                    afterSelect: function (values) {
                        $("input:radio[name='<?=$checkbox_name?>'][value=4]").iCheck('check');
                    },
                    afterDeselect: function (values) {
                        $("input:radio[name='<?=$checkbox_name?>'][value=4]").iCheck('check');
                    }
                });

                $("#input-group-departement input:checkbox").on("ifChecked", function(e) {
                    $("input:radio[name='<?=$checkbox_name?>'][value=5]").iCheck("check");
                });
            });
        </script>


    <?php
    }

    public static function recherche_alpha($base_url, $selected)
    {
        $title = RECHERCHE_APLHABETIQUE;
        echo "<div class='col-sm-3'>$title</div>";
        echo "<div class='col-sm-9 search-alphabetically m-bot15'>";
        if ($selected == '1')
            echo "<strong>#</strong>";
        else
            /** @noinspection PhpUndefinedVariableInspection */
            echo "<a href='$base_url?rechr_alpha=1&usid={$doorman->usid}'>#</a>";
        $quick_search = range('A', 'Z');
        foreach ($quick_search as $search) {
            $url = Route::relative($base_url, array("rechr_alpha" => strtolower($search)));
            if ($search == $selected)
                echo "<a href='#'><strong>$search</strong></a>";
            else
                echo "<a href='$url'>$search</a>";
        }
        echo "</div>";
    }

    public static function title($title, callable $inner = null)
    {
        echo "<div id='title-area-page'>";
        echo "  <h1 class='title-user'>$title</h1>";
        if ($inner != NULL) $inner();
        echo "  <div class='clear'></div>";
        echo "</div>";
    }

    /**
     * Requires jQuery, push a new script on page, escapes and json encode an array, creates an js object with it
     *
     * @param $array       array to jsonify (will be escaped to pass to a string)
     * @param $js_var_name string javascriptvar
     */
    public static function array_to_javascript_shame($array, $js_var_name)
    {
        ?>
        <script type="application/javascript" LANGUAGE="JavaScript">
            <?php echo "var " . $js_var_name . " = jQuery.parseJSON('" . mysql_real_escape_string(json_encode($array)) . "')"; ?>
        </script>
    <?php
    }

    /**
     * Replacement for enleveaccent($mot) in inc.fonctions.php
     *
     * @param $mot
     *
     * @return mixed
     */
    public static function enleveaccent($mot)
    {
        return self::_legacy_underscore_accents(self::_remove_accents($mot));
    }

    /**
     * Write the version tag, using the .version file at the document root.
     */
    public static function version_tag() {
        // each part as it class
        $tag_klass = array(
            "lowlight xs-hidden",
            NULL,
            NULL,
            "lowlight xs-hidden",
            "lowlight xs-hidden"
        );

        $version = shell_exec("sh ".$_SERVER['DOCUMENT_ROOT']."version.sh");
        $version_part = explode("-", $version);
        foreach($version_part as $i => &$p) {
            $p = "<span class='{$tag_klass[$i]}'>$p</span>";
        }
        return "<span class='version-tag'>" . implode("<span class='lowlight xs-hidden'>-</span>", $version_part) . "</span>";
    }

    private static function _legacy_underscore_accents($string)
    {
        $special_chars = array(chr(128), chr(63), chr(39), chr(38));
        $replacement = "_";
        return str_replace($special_chars, $replacement, $string);
    }

    /*function enleveaccent($mot) {
        $retour = "";
        $minus=strtolower($mot);
        for($i=0;$i<strlen($minus);$i++){
            $car=substr($minus,$i,1);
            $val=ord($car);
            if ($val==128){$car='_';} // point iinterrogation
            if ($val==63){$car='_';} // point iinterrogation
            if ($val==39){$car='_';} // apostrophe
            if ($val==38){$car='_';} // &
            if ($val>=224 && $val<=229){$car='a';}
            if ($val==231){$car='c';}
            if ($val>=232 && $val<=235){$car='e';}

            if ($val>=236 && $val<=239){$car='i';}
            if ($val==240 || ($val>=242 && $val<=246)){$car='o';}
            if ($val==241){$car='n';}
            if ($val>=249 && $val<=252){$car='u';}
            if ($val==253 || $val==255){$car='y';}

            if ($car=='è' || $car=='é' || $car=='ê' || $car=='ë'){$car='e';}
            if ($car=='à' || $car=='ä'){$car='â';}
            if ($car=='ç' ){$car='c';}


            $retour.=$car;
        }// fin for($i=0;$i<strlen($minus);$i++)
        $retour = strtr(strtolower($retour)," ","_"); // remplace l'espace parce un underscore
        $retour = strtr(strtolower($retour),"#",""); // remplace le # par rien
        return($retour);
    }*/

    /**
     * Converts all accent characters to ASCII characters.
     * If there are no accent characters, then the string given is just returned.
     *
     * @param string $string Text that might have accent characters
     *
     * @return string Filtered string with replaced "nice" characters.
     *
     * Replacement for old crappy enleveaccent function
     * taken from wordpress (https://raw.githubusercontent.com/WordPress/WordPress/master/wp-includes/formatting.php)
     * @licence GPLv2
     */
    private static function _remove_accents($string)
    {
        if (!preg_match('/[\x80-\xff]/', $string))
            return $string;

        if (self::_seems_utf8($string)) {
            $chars = array(
                // Decompositions for Latin-1 Supplement
                chr(194) . chr(170) => 'a', chr(194) . chr(186) => 'o',
                chr(195) . chr(128) => 'A', chr(195) . chr(129) => 'A',
                chr(195) . chr(130) => 'A', chr(195) . chr(131) => 'A',
                chr(195) . chr(132) => 'A', chr(195) . chr(133) => 'A',
                chr(195) . chr(134) => 'AE', chr(195) . chr(135) => 'C',
                chr(195) . chr(136) => 'E', chr(195) . chr(137) => 'E',
                chr(195) . chr(138) => 'E', chr(195) . chr(139) => 'E',
                chr(195) . chr(140) => 'I', chr(195) . chr(141) => 'I',
                chr(195) . chr(142) => 'I', chr(195) . chr(143) => 'I',
                chr(195) . chr(144) => 'D', chr(195) . chr(145) => 'N',
                chr(195) . chr(146) => 'O', chr(195) . chr(147) => 'O',
                chr(195) . chr(148) => 'O', chr(195) . chr(149) => 'O',
                chr(195) . chr(150) => 'O', chr(195) . chr(153) => 'U',
                chr(195) . chr(154) => 'U', chr(195) . chr(155) => 'U',
                chr(195) . chr(156) => 'U', chr(195) . chr(157) => 'Y',
                chr(195) . chr(158) => 'TH', chr(195) . chr(159) => 's',
                chr(195) . chr(160) => 'a', chr(195) . chr(161) => 'a',
                chr(195) . chr(162) => 'a', chr(195) . chr(163) => 'a',
                chr(195) . chr(164) => 'a', chr(195) . chr(165) => 'a',
                chr(195) . chr(166) => 'ae', chr(195) . chr(167) => 'c',
                chr(195) . chr(168) => 'e', chr(195) . chr(169) => 'e',
                chr(195) . chr(170) => 'e', chr(195) . chr(171) => 'e',
                chr(195) . chr(172) => 'i', chr(195) . chr(173) => 'i',
                chr(195) . chr(174) => 'i', chr(195) . chr(175) => 'i',
                chr(195) . chr(176) => 'd', chr(195) . chr(177) => 'n',
                chr(195) . chr(178) => 'o', chr(195) . chr(179) => 'o',
                chr(195) . chr(180) => 'o', chr(195) . chr(181) => 'o',
                chr(195) . chr(182) => 'o', chr(195) . chr(184) => 'o',
                chr(195) . chr(185) => 'u', chr(195) . chr(186) => 'u',
                chr(195) . chr(187) => 'u', chr(195) . chr(188) => 'u',
                chr(195) . chr(189) => 'y', chr(195) . chr(190) => 'th',
                chr(195) . chr(191) => 'y', chr(195) . chr(152) => 'O',
                // Decompositions for Latin Extended-A
                chr(196) . chr(128) => 'A', chr(196) . chr(129) => 'a',
                chr(196) . chr(130) => 'A', chr(196) . chr(131) => 'a',
                chr(196) . chr(132) => 'A', chr(196) . chr(133) => 'a',
                chr(196) . chr(134) => 'C', chr(196) . chr(135) => 'c',
                chr(196) . chr(136) => 'C', chr(196) . chr(137) => 'c',
                chr(196) . chr(138) => 'C', chr(196) . chr(139) => 'c',
                chr(196) . chr(140) => 'C', chr(196) . chr(141) => 'c',
                chr(196) . chr(142) => 'D', chr(196) . chr(143) => 'd',
                chr(196) . chr(144) => 'D', chr(196) . chr(145) => 'd',
                chr(196) . chr(146) => 'E', chr(196) . chr(147) => 'e',
                chr(196) . chr(148) => 'E', chr(196) . chr(149) => 'e',
                chr(196) . chr(150) => 'E', chr(196) . chr(151) => 'e',
                chr(196) . chr(152) => 'E', chr(196) . chr(153) => 'e',
                chr(196) . chr(154) => 'E', chr(196) . chr(155) => 'e',
                chr(196) . chr(156) => 'G', chr(196) . chr(157) => 'g',
                chr(196) . chr(158) => 'G', chr(196) . chr(159) => 'g',
                chr(196) . chr(160) => 'G', chr(196) . chr(161) => 'g',
                chr(196) . chr(162) => 'G', chr(196) . chr(163) => 'g',
                chr(196) . chr(164) => 'H', chr(196) . chr(165) => 'h',
                chr(196) . chr(166) => 'H', chr(196) . chr(167) => 'h',
                chr(196) . chr(168) => 'I', chr(196) . chr(169) => 'i',
                chr(196) . chr(170) => 'I', chr(196) . chr(171) => 'i',
                chr(196) . chr(172) => 'I', chr(196) . chr(173) => 'i',
                chr(196) . chr(174) => 'I', chr(196) . chr(175) => 'i',
                chr(196) . chr(176) => 'I', chr(196) . chr(177) => 'i',
                chr(196) . chr(178) => 'IJ', chr(196) . chr(179) => 'ij',
                chr(196) . chr(180) => 'J', chr(196) . chr(181) => 'j',
                chr(196) . chr(182) => 'K', chr(196) . chr(183) => 'k',
                chr(196) . chr(184) => 'k', chr(196) . chr(185) => 'L',
                chr(196) . chr(186) => 'l', chr(196) . chr(187) => 'L',
                chr(196) . chr(188) => 'l', chr(196) . chr(189) => 'L',
                chr(196) . chr(190) => 'l', chr(196) . chr(191) => 'L',
                chr(197) . chr(128) => 'l', chr(197) . chr(129) => 'L',
                chr(197) . chr(130) => 'l', chr(197) . chr(131) => 'N',
                chr(197) . chr(132) => 'n', chr(197) . chr(133) => 'N',
                chr(197) . chr(134) => 'n', chr(197) . chr(135) => 'N',
                chr(197) . chr(136) => 'n', chr(197) . chr(137) => 'N',
                chr(197) . chr(138) => 'n', chr(197) . chr(139) => 'N',
                chr(197) . chr(140) => 'O', chr(197) . chr(141) => 'o',
                chr(197) . chr(142) => 'O', chr(197) . chr(143) => 'o',
                chr(197) . chr(144) => 'O', chr(197) . chr(145) => 'o',
                chr(197) . chr(146) => 'OE', chr(197) . chr(147) => 'oe',
                chr(197) . chr(148) => 'R', chr(197) . chr(149) => 'r',
                chr(197) . chr(150) => 'R', chr(197) . chr(151) => 'r',
                chr(197) . chr(152) => 'R', chr(197) . chr(153) => 'r',
                chr(197) . chr(154) => 'S', chr(197) . chr(155) => 's',
                chr(197) . chr(156) => 'S', chr(197) . chr(157) => 's',
                chr(197) . chr(158) => 'S', chr(197) . chr(159) => 's',
                chr(197) . chr(160) => 'S', chr(197) . chr(161) => 's',
                chr(197) . chr(162) => 'T', chr(197) . chr(163) => 't',
                chr(197) . chr(164) => 'T', chr(197) . chr(165) => 't',
                chr(197) . chr(166) => 'T', chr(197) . chr(167) => 't',
                chr(197) . chr(168) => 'U', chr(197) . chr(169) => 'u',
                chr(197) . chr(170) => 'U', chr(197) . chr(171) => 'u',
                chr(197) . chr(172) => 'U', chr(197) . chr(173) => 'u',
                chr(197) . chr(174) => 'U', chr(197) . chr(175) => 'u',
                chr(197) . chr(176) => 'U', chr(197) . chr(177) => 'u',
                chr(197) . chr(178) => 'U', chr(197) . chr(179) => 'u',
                chr(197) . chr(180) => 'W', chr(197) . chr(181) => 'w',
                chr(197) . chr(182) => 'Y', chr(197) . chr(183) => 'y',
                chr(197) . chr(184) => 'Y', chr(197) . chr(185) => 'Z',
                chr(197) . chr(186) => 'z', chr(197) . chr(187) => 'Z',
                chr(197) . chr(188) => 'z', chr(197) . chr(189) => 'Z',
                chr(197) . chr(190) => 'z', chr(197) . chr(191) => 's',
                // Decompositions for Latin Extended-B
                chr(200) . chr(152) => 'S', chr(200) . chr(153) => 's',
                chr(200) . chr(154) => 'T', chr(200) . chr(155) => 't',
                // Euro Sign
                chr(226) . chr(130) . chr(172) => 'E',
                // GBP (Pound) Sign
                chr(194) . chr(163) => '',
                // Vowels with diacritic (Vietnamese)
                // unmarked
                chr(198) . chr(160) => 'O', chr(198) . chr(161) => 'o',
                chr(198) . chr(175) => 'U', chr(198) . chr(176) => 'u',
                // grave accent
                chr(225) . chr(186) . chr(166) => 'A', chr(225) . chr(186) . chr(167) => 'a',
                chr(225) . chr(186) . chr(176) => 'A', chr(225) . chr(186) . chr(177) => 'a',
                chr(225) . chr(187) . chr(128) => 'E', chr(225) . chr(187) . chr(129) => 'e',
                chr(225) . chr(187) . chr(146) => 'O', chr(225) . chr(187) . chr(147) => 'o',
                chr(225) . chr(187) . chr(156) => 'O', chr(225) . chr(187) . chr(157) => 'o',
                chr(225) . chr(187) . chr(170) => 'U', chr(225) . chr(187) . chr(171) => 'u',
                chr(225) . chr(187) . chr(178) => 'Y', chr(225) . chr(187) . chr(179) => 'y',
                // hook
                chr(225) . chr(186) . chr(162) => 'A', chr(225) . chr(186) . chr(163) => 'a',
                chr(225) . chr(186) . chr(168) => 'A', chr(225) . chr(186) . chr(169) => 'a',
                chr(225) . chr(186) . chr(178) => 'A', chr(225) . chr(186) . chr(179) => 'a',
                chr(225) . chr(186) . chr(186) => 'E', chr(225) . chr(186) . chr(187) => 'e',
                chr(225) . chr(187) . chr(130) => 'E', chr(225) . chr(187) . chr(131) => 'e',
                chr(225) . chr(187) . chr(136) => 'I', chr(225) . chr(187) . chr(137) => 'i',
                chr(225) . chr(187) . chr(142) => 'O', chr(225) . chr(187) . chr(143) => 'o',
                chr(225) . chr(187) . chr(148) => 'O', chr(225) . chr(187) . chr(149) => 'o',
                chr(225) . chr(187) . chr(158) => 'O', chr(225) . chr(187) . chr(159) => 'o',
                chr(225) . chr(187) . chr(166) => 'U', chr(225) . chr(187) . chr(167) => 'u',
                chr(225) . chr(187) . chr(172) => 'U', chr(225) . chr(187) . chr(173) => 'u',
                chr(225) . chr(187) . chr(182) => 'Y', chr(225) . chr(187) . chr(183) => 'y',
                // tilde
                chr(225) . chr(186) . chr(170) => 'A', chr(225) . chr(186) . chr(171) => 'a',
                chr(225) . chr(186) . chr(180) => 'A', chr(225) . chr(186) . chr(181) => 'a',
                chr(225) . chr(186) . chr(188) => 'E', chr(225) . chr(186) . chr(189) => 'e',
                chr(225) . chr(187) . chr(132) => 'E', chr(225) . chr(187) . chr(133) => 'e',
                chr(225) . chr(187) . chr(150) => 'O', chr(225) . chr(187) . chr(151) => 'o',
                chr(225) . chr(187) . chr(160) => 'O', chr(225) . chr(187) . chr(161) => 'o',
                chr(225) . chr(187) . chr(174) => 'U', chr(225) . chr(187) . chr(175) => 'u',
                chr(225) . chr(187) . chr(184) => 'Y', chr(225) . chr(187) . chr(185) => 'y',
                // acute accent
                chr(225) . chr(186) . chr(164) => 'A', chr(225) . chr(186) . chr(165) => 'a',
                chr(225) . chr(186) . chr(174) => 'A', chr(225) . chr(186) . chr(175) => 'a',
                chr(225) . chr(186) . chr(190) => 'E', chr(225) . chr(186) . chr(191) => 'e',
                chr(225) . chr(187) . chr(144) => 'O', chr(225) . chr(187) . chr(145) => 'o',
                chr(225) . chr(187) . chr(154) => 'O', chr(225) . chr(187) . chr(155) => 'o',
                chr(225) . chr(187) . chr(168) => 'U', chr(225) . chr(187) . chr(169) => 'u',
                // dot below
                chr(225) . chr(186) . chr(160) => 'A', chr(225) . chr(186) . chr(161) => 'a',
                chr(225) . chr(186) . chr(172) => 'A', chr(225) . chr(186) . chr(173) => 'a',
                chr(225) . chr(186) . chr(182) => 'A', chr(225) . chr(186) . chr(183) => 'a',
                chr(225) . chr(186) . chr(184) => 'E', chr(225) . chr(186) . chr(185) => 'e',
                chr(225) . chr(187) . chr(134) => 'E', chr(225) . chr(187) . chr(135) => 'e',
                chr(225) . chr(187) . chr(138) => 'I', chr(225) . chr(187) . chr(139) => 'i',
                chr(225) . chr(187) . chr(140) => 'O', chr(225) . chr(187) . chr(141) => 'o',
                chr(225) . chr(187) . chr(152) => 'O', chr(225) . chr(187) . chr(153) => 'o',
                chr(225) . chr(187) . chr(162) => 'O', chr(225) . chr(187) . chr(163) => 'o',
                chr(225) . chr(187) . chr(164) => 'U', chr(225) . chr(187) . chr(165) => 'u',
                chr(225) . chr(187) . chr(176) => 'U', chr(225) . chr(187) . chr(177) => 'u',
                chr(225) . chr(187) . chr(180) => 'Y', chr(225) . chr(187) . chr(181) => 'y',
                // Vowels with diacritic (Chinese, Hanyu Pinyin)
                chr(201) . chr(145) => 'a',
                // macron
                chr(199) . chr(149) => 'U', chr(199) . chr(150) => 'u',
                // acute accent
                chr(199) . chr(151) => 'U', chr(199) . chr(152) => 'u',
                // caron
                chr(199) . chr(141) => 'A', chr(199) . chr(142) => 'a',
                chr(199) . chr(143) => 'I', chr(199) . chr(144) => 'i',
                chr(199) . chr(145) => 'O', chr(199) . chr(146) => 'o',
                chr(199) . chr(147) => 'U', chr(199) . chr(148) => 'u',
                chr(199) . chr(153) => 'U', chr(199) . chr(154) => 'u',
                // grave accent
                chr(199) . chr(155) => 'U', chr(199) . chr(156) => 'u',
            );


            $string = strtr($string, $chars);
        } else {
            // Assume ISO-8859-1 if not UTF-8
            $chars['in'] = chr(128) . chr(131) . chr(138) . chr(142) . chr(154) . chr(158)
                . chr(159) . chr(162) . chr(165) . chr(181) . chr(192) . chr(193) . chr(194)
                . chr(195) . chr(196) . chr(197) . chr(199) . chr(200) . chr(201) . chr(202)
                . chr(203) . chr(204) . chr(205) . chr(206) . chr(207) . chr(209) . chr(210)
                . chr(211) . chr(212) . chr(213) . chr(214) . chr(216) . chr(217) . chr(218)
                . chr(219) . chr(220) . chr(221) . chr(224) . chr(225) . chr(226) . chr(227)
                . chr(228) . chr(229) . chr(231) . chr(232) . chr(233) . chr(234) . chr(235)
                . chr(236) . chr(237) . chr(238) . chr(239) . chr(241) . chr(242) . chr(243)
                . chr(244) . chr(245) . chr(246) . chr(248) . chr(249) . chr(250) . chr(251)
                . chr(252) . chr(253) . chr(255);

            $chars['out'] = "EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy";

            $string = strtr($string, $chars['in'], $chars['out']);
            $double_chars['in'] = array(chr(140), chr(156), chr(198), chr(208), chr(222), chr(223), chr(230), chr(240), chr(254));
            $double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th');
            $string = str_replace($double_chars['in'], $double_chars['out'], $string);
        }

        return $string;
    }

    /**
     * Checks to see if a string is utf8 encoded.
     * NOTE: This function checks for 5-Byte sequences, UTF8
     *       has Bytes Sequences with a maximum length of 4.
     * @author  bmorel at ssi dot fr (modified)
     * @since   1.2.1
     *
     * @param string $str The string to be checked
     *
     * @return bool True if $str fits a UTF-8 model, false otherwise.
     *
     * Replacement for old crappy enleveaccent function
     * taken from wordpress (https://raw.githubusercontent.com/WordPress/WordPress/master/wp-includes/formatting.php)
     * @licence GPLv2
     */
    private static function _seems_utf8($str)
    {
        self::_mbstring_binary_safe_encoding();
        $length = strlen($str);
        self::_reset_mbstring_encoding();
        for ($i = 0; $i < $length; $i++) {
            $c = ord($str[$i]);
            if ($c < 0x80) $n = 0; # 0bbbbbbb
            elseif (($c & 0xE0) == 0xC0) $n = 1; # 110bbbbb
            elseif (($c & 0xF0) == 0xE0) $n = 2; # 1110bbbb
            elseif (($c & 0xF8) == 0xF0) $n = 3; # 11110bbb
            elseif (($c & 0xFC) == 0xF8) $n = 4; # 111110bb
            elseif (($c & 0xFE) == 0xFC) $n = 5; # 1111110b
            else return false; # Does not match any model
            for ($j = 0; $j < $n; $j++) { # n bytes matching 10bbbbbb follow ?
                if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
                    return false;
            }
        }
        return true;
    }

    /**
     * @param bool $reset
     *
     * Replacement for old crappy enleveaccent function
     * taken from wordpress (https://raw.githubusercontent.com/WordPress/WordPress/master/wp-includes/formatting.php)
     *
     * @licence GPLv2
     */
    private static function _mbstring_binary_safe_encoding($reset = false)
    {
        static $encodings = array();
        static $overloaded = null;

        if (is_null($overloaded))
            $overloaded = function_exists('mb_internal_encoding') && (ini_get('mbstring.func_overload') & 2);

        if (false === $overloaded)
            return;

        if (!$reset) {
            $encoding = mb_internal_encoding();
            array_push($encodings, $encoding);
            mb_internal_encoding('ISO-8859-1');
        }

        if ($reset && $encodings) {
            $encoding = array_pop($encodings);
            mb_internal_encoding($encoding);
        }
    }

    /**
     * @licence GPLv2
     * Replacement for old crappy enleveaccent function
     * taken from wordpress (https://raw.githubusercontent.com/WordPress/WordPress/master/wp-includes/formatting.php)
     * @licence GPLv2
     */
    private static function _reset_mbstring_encoding()
    {
        self::_mbstring_binary_safe_encoding(true);
    }

    public static function CKEditor($params)
    {
        if ($params['id'] == '' || $params['width'] == '' || $params['height'] == '') {
            echo 'Paramètres invalides ou manquants';
            return false;
        }

        if (!self::_is_compatible_browser()) {
            //Remplacer les <br> par des \r\n
            $params['value'] = str_replace(array("</p>", "<br />"), "\r\n", $params['value']);
            $params['value'] = strip_tags($params['value']);

            $nb_rows = ($params['height'] / 20);
            $nb_cols = ($params['width'] / 8);

            ?>
            <textarea style="width:<?= $params['width'] ?>px; height:<?= $params['height'] ?>px;" id="<?= $params['id'] ?>" name="<?= $params['id'] ?>" rows="<?= $nb_rows ?>" cols="<?= $nb_cols ?>"><?= $params['value'] ?></textarea>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#<?=$params['id']?>").autoGrow();
                });
            </script>
        <?php
        } else {
            $params['value'] = preg_replace('/(<style[^>]*>\\s*)<!--(.*)-->(\\s*<\\/style>)/is', '$1$2$3', $params['value']);
            ?>
            <textarea style="width:<?= $params['width'] ?>px; height:<?= $params['height'] ?>px;" id="<?= $params['id'] ?>" name="<?= $params['id'] ?>"><?= $params['value'] ?></textarea>
            <script type="text/javascript">
                var editor = CKEDITOR.replace('<?=$params['id']?>',
                    {
                        height: '<?=$params['height']?>', width: '<?=$params['width']?>', resize_enabled: false, toolbar: 'simple'<?php if($params['code_langue'] != ''){ ?>, language: '<?=$params['code_langue']?>'<?php } ?> <?php if($params['content_css'] != ''){ ?>, contentsCss: '<?=$params['content_css']?>'<?php } ?>
                    });
            </script>
        <?php
        }
    }

    private static function _is_compatible_browser()
    {
        if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPod') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad') || strstr($_SERVER['HTTP_USER_AGENT'], 'android') || strstr($_SERVER['HTTP_USER_AGENT'], 'palm'))
            return false;
        else
            return true;
    }
}
