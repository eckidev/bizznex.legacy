<?php
/**
 * Created by JetBrains PhpStorm.
 * User: micael.bergeron
 * Date: 08/07/14
 * Time: 9:20 AM
 * To change this template use File | Settings | File Templates.
 */

namespace Bizznex\Utils;

class Route {

    const TUTORIAL_PATH = "http://www.bizznex.ca/";

    public static function relative($href, array $params = array())
    {
        $params['usid'] = $_GET['usid'];
        return $href."?".http_build_query($params);
    }

    public static function absolute($href, array $params = array())
    {
        $params['usid'] = $_GET['usid'];
        return SITEPATH.$href."?".http_build_query($params);
    }

    public static function tutorial($href, array $params = array())
    {
        $lang = substr($_SESSION[$_GET['usid']]['LANGUE'], 0, 2);
        $url = self::TUTORIAL_PATH."$lang/$href";
        if (count($params) > 0)
          $url .= "?".http_build_query($params);
        return $url;
    }
}
