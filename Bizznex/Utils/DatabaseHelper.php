<?php
/**
 * Created by IntelliJ IDEA.
 * User: gabriel.LeBreton
 * Date: 10/1/2014
 * Time: 11:56 AM
 */

namespace Bizznex\Utils;


class DatabaseHelper {
    /**
     * @param $data string looking like ?1|2|3|4? or ?1?
     *
     * @return array|string
     */
    public static function get_questionmark_pipe_separated_values($data) {
        return explode("|", str_replace("?", "", $data));
    }

    /**
     * @param $data string looking like @1@@2@@3@@4@ or @1@
     *
     * @return array
     */
    public static function get_atsign_separated_values($data) {
        return array_map(function($data) {
            return str_replace("@", "", $data);
        }, explode("@@", $data));
    }

    /**
     * @param $data string looking like 1&2&3&
     * @return array
     */
    public static function get_ampersand_separated_values($data) {
        return array_filter(explode("&", $data));
    }

    public static function to_questionmark_pipe_separated_values(array $data) {
        return "?".implode("|", $data)."?";
    }

    public static function to_atsign_separated_values(array $data) {
        return "@".implode("@@", $data)."@";
    }

    public static function to_ampersand_separated_values(array $data) {
        return implode("&", $data)."&";
    }

    /**
     * return bool
     */
    public static function recipients_has_read_task($task_id)
    {
        $req_task = mysql_query("SELECT * FROM t_tache WHERE id_tache = '" . $task_id . "'");
        while ($task_data = mysql_fetch_array($req_task)) {
            $recipients = self::get_questionmark_pipe_separated_values($task_data['utilisateurs_id_tache']);
            $has_read_task_recipients = self::get_atsign_separated_values($task_data['lu_tache']);
            foreach ($recipients as $recipient) {
                if (!in_array($recipient, $has_read_task_recipients)) {
                    return false;
                }
            }
        }
        return true;
    }
} 